FROM node:9.11.2
RUN mkdir /code
RUN mkdir /code/Frontend
COPY ./code /code/Frontend
RUN npm -g config set user root
RUN npm install -g node-sass
WORKDIR /code/Frontend
EXPOSE 8080
