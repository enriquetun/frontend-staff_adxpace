import Vue from 'vue'
import Router from 'vue-router'
import AsistentePromotores from '@/components/AsistentePromotores'
import AsistentePropietario from '@/components/AsistentePropietario'
import AsistenteLocal from '@/components/AsistenteLocal'
import AsistenteEspacio from '@/components/AsistenteEspacio'
import AsistenteInstitucional from '@/components/AsistenteInstitucional'
import AsistenteEditPropietario from '@/components/AsistenteEditPropietario'
import AsistenteEditLocal from '@/components/AsistenteEditLocal'
import AsistenteEditEspacio from '@/components/AsistenteEditEspacio'
import AsistenteEditEspacioInstitucional from '@/components/AsistenteEditEspacioInstitucional'
import AsistenteEditPropietarioBancario from '@/components/AsistenteEditPropietarioBancario'
import Extras from '@/components/Extras'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Promotores',
      component: AsistentePromotores
    },
    {
      path: '/propietario',
      name: 'Propietario',
      component: AsistentePropietario
    },
    {
      path: '/local',
      name: 'Local',
      component: AsistenteLocal
    },
    {
      path: '/espacio',
      name: 'Espacio',
      component: AsistenteEspacio
    },
    {
      path: '/institucionales',
      name: 'Institucional',
      component: AsistenteInstitucional
    },
    {
      path: '/extras',
      name: 'Extras',
      component: Extras
    },
    {
      path: '/propietario/edit',
      name: 'PropietarioEdit',
      component: AsistenteEditPropietario
    },
    {
      path: '/local/edit',
      name: 'LocalEdit',
      component: AsistenteEditLocal
    },
    {
      path: '/espacio/edit',
      name: 'EspacioEdit',
      component: AsistenteEditEspacio
    },
    {
      path: '/propietario/datosbancarios/edit',
      name: 'PropietarioBancariosEdit',
      component: AsistenteEditPropietarioBancario
    },
    {
      path: '/institucionales/edit',
      name: 'AsistenteEditEspacioInstitucional',
      component: AsistenteEditEspacioInstitucional
    }
  ]
})
