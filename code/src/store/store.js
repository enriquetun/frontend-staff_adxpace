import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/store/api'
import moment from 'moment'
import options from '@/store/options'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    // apiRoot: 'https://pilot.adxpace.com/',
    apiRoot: 'https://adxpace.test/',
    table_options: options.table_options,
    data_promotores: [],
    data_propietarios: false,
    data_espacios: false,
    data_locales: false,
    data_institucionales: false,
    data_locales_no_asignado: false,
    data_giros: [{'pk': 10, 'designacion': 'CAFE'}, {'pk': 23, 'designacion': 'CAMIONES'}, {'pk': 13, 'designacion': 'CARNICERIA/POLLERIA'}],
    data_tipos: [{'pk': 5, 'designacion': 'CARTELERA'}, {'pk': 6, 'designacion': 'ESPECTACULAR'}, {'pk': 1, 'designacion': 'PARED'}],
    data_materiales: [],
    data_propietariosEspacios: [],
    data_propietariosLocales: [],
    data_propietario: [],
    codigos_postales: {
      colonias: [],
      municipio: '',
      estado: ''
    }
  },
  getters: {
    propietario_espacio: (state) => {
      let propietarioEspacio = ['']
      let k = 0
      for (var i = 0; i < state.data_propietariosEspacios.length; i++) {
        let espacio = state.data_propietariosEspacios[i]
        propietarioEspacio[k++] = {
          'pk': espacio.pk,
          'i_espacio': i,
          'nombre': espacio.nombre,
          'registro': moment(espacio.registro),
          'giro': espacio.giro[1],
          'foto': espacio.foto
        }
      }
      return propietarioEspacio
    },
    getter_datos_bancarios: (state) => {
      let datosbancarios
      if (state.data_propietario.datos_bancarios) {
        datosbancarios = state.data_propietario.datos_bancarios
      } else {
        datosbancarios = null
      }
      return datosbancarios
    },
    getter_datos_comerciales: (state) => {
      let datosComerciales
      if (state.data_propietario.datos_comerciales) {
        datosComerciales = state.data_propietario.datos_comerciales
      } else {
        datosComerciales = null
      }
      return datosComerciales
    }
  },
  actions: {
    // ---------CARGA DE DATOS---------------------
    async asistenteData (store) {
      // await store.dispatch('asistentePropietarios', '?')
      await store.dispatch('asistentePromotores')
      await store.dispatch('giros')
      await store.dispatch('tipos')
      await store.dispatch('materiales')
    },
    asistentePromotores ({state, commit}) {
      var url = state.apiRoot + 'staff/promotores'
      return api.get(url)
        .then((response) => commit('ASISTENTE_PROMOTOR_GET', response.body))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    async asistentePropietarios ({state, commit}, data) {
      var url
      try {
        url = new URL(data)
        url = data
      } catch (error) {
        url = state.apiRoot + 'staff/propietarios'
      }
      return api.get(url)
        .then((response) => {
          var dataPropietario = response.body
          for (let i = 0; i < dataPropietario.results.length; i++) {
            if (dataPropietario.results[i].usuario) {
              dataPropietario.results[i].usuario = dataPropietario.results[i].usuario.username
            } else {
              dataPropietario.results[i].usuario = ''
            }
            if (!dataPropietario.results[i].promotor.datos_personales) {
              dataPropietario.results[i].promotor.datos_personales = ''
            }
            dataPropietario.results[i].fecha_de_registro = moment(dataPropietario.results[i].fecha_de_registro)
          }
          store.commit('ASISTENTE_PROPIETARIO_GET', dataPropietario)
        })
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    async searchInstitucionales ({state, commit}, search) {
      var url
      try {
        url = new URL(search)
        url = search
      } catch (error) {
        url = state.apiRoot + '/vendedor/espacios_institucionales/' + search
      }
      return api.get(url)
        .then((response) => {
          store.commit('INSTITUCIONALES_SEARCH', response)
        }).catch((error) => commit('API_FAIL', error))
    },
    async searchPropietarios ({state, commit}, search) {
      var url
      try {
        url = new URL(search)
        url = search
      } catch (error) {
        url = state.apiRoot + 'staff/propietarios/' + search
      }
      return api.get(url).then((response) => {
        var dataPropietario = response.body
        for (let i = 0; i < dataPropietario.results.length; i++) {
          if (dataPropietario.results[i].usuario) {
            dataPropietario.results[i].usuario = dataPropietario.results[i].usuario.username
          } else {
            dataPropietario.results[i].usuario = ''
          }
          if (!dataPropietario.results[i].promotor.datos_personales) {
            dataPropietario.results[i].promotor.datos_personales = ''
          }
          dataPropietario.results[i].fecha_de_registro = moment(dataPropietario.results[i].fecha_de_registro)
        }
        store.commit('ASISTENTE_PROPIETARIO_GET', dataPropietario)
      }).catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    async searchEspacios ({state, commit}, search) {
      var url
      try {
        url = new URL(search)
        url = search
      } catch (error) {
        url = state.apiRoot + 'staff/espacios/' + search
      }

      return api.get(url)
        .then((response) => {
          var dataEspacios = response.body
          for (var n = 0; n < dataEspacios.results.length; n++) {
            dataEspacios.results[n].fecha_de_registro = moment(dataEspacios.results[n].fecha_de_registro)
          }
          store.commit('ASISTENTE_ESPACIO_GET', dataEspacios)
        }).catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    async searchLocales ({state, commit}, search) {
      var url
      try {
        url = new URL(search)
        url = search
      } catch (error) {
        url = state.apiRoot + 'staff/locales/' + search
      }
      return api.get(url)
        .then((response) => {
          var dataLocales = response.body
          for (var n = 0; n < dataLocales.results.length; n++) {
            if (dataLocales.results[n].foto) {
              dataLocales.results[n].foto = dataLocales.results[n].foto.src
            } else {
              dataLocales.results[n].foto = ''
            }
            if (dataLocales.results[n].propietario.usuario) {
              dataLocales.results[n].propietario.usuario = dataLocales.results[n].propietario.usuario.username
            } else {
              dataLocales.results[n].propietario.usuario = ''
            }
            dataLocales.results[n].fecha_de_registro = moment(dataLocales.results[n].fecha_de_registro)
          }
          store.commit('ASISTENTE_LOCALES_GET', dataLocales)
        }).catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    async giros ({state, commit}) {
      var url = state.apiRoot + 'comun/giros'
      return api.get(url)
        .then((response) => commit('ASISTENTE_GIRO', response.body))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    async tipos ({state, commit}) {
      var url = state.apiRoot + 'comun/tipos'
      return api.get(url)
        .then((response) => commit('ASISTENTE_TIPO', response.body))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    async materiales ({state, commit}) {
      var url = state.apiRoot + 'comun/materiales'
      return api.get(url)
        .then((response) => commit('ASISTENTE_MATERIAL', response.body))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    async locales_no_asignados ({state, commit}) {
      var url = state.apiRoot + 'staff/locales_sin_giro'
      return api.get(url)
        .then((response) => commit('ASISTENTE_NO_ASIGNADO', response))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    // ----BUSQUEDAS DESDE EL FRONTEND-------------
    asistentePropietario ({state, commit}, pk) {
      var url = state.apiRoot + 'staff/propietarios/' + pk
      return api.get(url)
        .then((response) => {
          commit('ASISTENTE_PROPIETARIO_PK', response.body)
        })
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    codigoPostal ({state, commit}, CodigoPostal) {
      var url = state.apiRoot + 'api/codigos_postales/v2/codigo_postal/' + CodigoPostal
      return api.get(url)
        .then((response) => store.commit('CODIGO_POSTAL', response))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    // -------------------------CARGAR DE TABLAS EN PROPIETARIO-----------------------
    PropietarioEspacios ({state, commit}, pk) {
      var url = state.apiRoot + 'staff/propietarios/espacios/' + pk
      return api.get(url)
        .then((response) => {
          store.commit('PROPIETARIO_DATA_ESPACIO', response.body)
        })
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    PropietarioLocal ({state, commit}, pk) {
      var url = state.apiRoot + 'staff/propietarios/locales/' + pk
      return api.get(url)
        .then((response) => {
          store.commit('PROPIETARIO_DATA_LOCAL', response.body)
        })
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    // -------------EDITAR LOCAL Y ESPACIO ESPECIFICO----------
    asistenteEditarEspacio ({state, commit}, pk) {
      var url = state.apiRoot + 'staff/espacios/' + pk
      return api.get(url)
        .then((response) => {
          store.commit('ASISTENTE_ESPACIO_EDITAR', response)
        })
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    asistenteEditarLocal ({state, commit}, pk) {
      var url = state.apiRoot + 'staff/locales/' + pk
      return api.get(url)
        .then((response) => {
          store.commit('PROPIETARIO_LOCAL_EDITAR', response.body)
        })
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    // ----------------PATCH DE PROPIETARIO -------------
    async PropietarioPost ({state, commit}, propietario) {
      var url = state.apiRoot + 'staff/propietarios/' + propietario.pk
      return api.patch(url, propietario)
        .then((response) => store.commit('DATOS_PERSONALES_RESPONSE', response))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    // -----------------PATCH DE ESPACIO ----------------------------
    async EspacioPatch ({state, commit}, espacio) {
      var url = state.apiRoot + 'staff/espacios/' + espacio.pk
      return api.patch(url, espacio.data)
        .then((response) => store.commit('ESPACIO_RESPONSE', response))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    async EspacioPatchCaracteristicas ({state, commit}, caracteristicas) {
      var url = state.apiRoot + 'staff/caracteristicas_fisicas/' + caracteristicas.pk_caracteristicas
      return api.patch(url, caracteristicas.data)
        .then((response) => store.commit('CARACTERISTICAS_FISICAS_RESPONSE', response))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    // DELETE
    async deletePropietario ({state, commit}, data) {
      var url = state.apiRoot + '/staff/'
      return api.delete(url)
        .then((response) => console.log(response))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    async deleteLocal ({state, commit}, data) {
      var url = state.apiRoot + '/staff/'
      return api.delete(url)
        .then((response) => console.log(response))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    async deleteEspacio ({state, commit}, data) {
      var url = state.apiRoot + '/staff/'
      return api.delete(url)
        .then((response) => console.log(response))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    async deleteInstitucional ({state, commit}, data) {
      var url = state.apiRoot + '/staff/'
      return api.delete(url)
        .then((response) => console.log(response))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },

    // -----------------PATCH DE LOCAL----------------------------
    async LocalPost ({state, commit}, local) {
      var url = state.apiRoot + 'staff/locales/' + local.pk
      return api.patch(url, local.data)
        .then((response) => store.commit('LOCAL_RESPONSE', response))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    async LocalPostImagen ({state, commit}, localImagen) {
      var url = state.apiRoot + 'staff/locales/' + localImagen.pk
      return api.patch(url, localImagen.data)
        .then((response) => store.commit('LOCAL_IMAGE_RESPONSE', response))
        .catch((error) => commit('ASISTENTE_API_FAIL', error))
    }
  },
  mutations: {
    // ------------GET------------
    ASISTENTE_PROMOTOR_GET: function (state, response) {
      state.data_promotores = response
    },
    ASISTENTE_PROPIETARIO_GET: function (state, dataPropietario) {
      state.data_propietarios = dataPropietario
    },
    ASISTENTE_ESPACIO_GET: function (state, dataEspacios) {
      state.data_espacios = dataEspacios
    },
    ASISTENTE_LOCALES_GET: function (state, dataLocales) {
      state.data_locales = dataLocales
    },
    INSTITUCIONALES_SEARCH: function (state, response) {
      state.data_institucionales = response.body
    },
    ASISTENTE_GIRO: function (state, response) {
      state.data_giros = response
    },
    ASISTENTE_TIPO: function (state, response) {
      state.data_tipos = response
    },
    ASISTENTE_MATERIAL: function (state, response) {
      state.data_materiales = response
    },
    ASISTENTE_NO_ASIGNADO: function (state, response) {
      state.data_locales_no_asignado = response
    },
    CODIGO_POSTAL: function (state, response) {
      state.codigos_postales.colonias = response.body.colonias
      state.codigos_postales.estado = response.body.estado
      state.codigos_postales.municipio = response.body.municipio
    },
    // ------------Propietario-----------
    ASISTENTE_PROPIETARIO_PK: function (state, dataPropietario) {
      state.data_propietario = dataPropietario
    },
    PROPIETARIO_DATA_ESPACIO: function (state, response) {
      state.data_propietariosEspacios = response
    },
    PROPIETARIO_DATA_LOCAL: function (state, response) {
      state.data_propietariosLocales = response
    },
    ASISTENTE_ESPACIO_EDITAR: function (state, response) {
      state.edit_espacio = response.body
    },
    PROPIETARIO_LOCAL_EDITAR: function (state, response) {
      state.edit_local = response
    },
    // ---------Variables----------
    RESET_TABLE_PROPIETARIOS: function (state) {
      state.data_propietario = []
    },
    RESET_TABLE_LOCAL: function (state) {
      state.data_locales = []
    },
    RESET_TABLE_ESPACIOS: function (state) {
      state.data_espacios = []
    },
    RESET_TABLE_INSTITUCIONALES: function (state) {
      state.data_institucionales = []
    },
    RESET_TABLE_LOCAL_EXTRA: function (state) {
      state.data_locales_no_asignado = []
    },
    // ----------POST---------
    DATOS_PERSONALES_RESPONSE: function (state, response) {
      if (response.ok === true) {
        console.log('actualizado')
      }
    },
    CARACTERISTICAS_FISICAS_RESPONSE: function (state, response) {
    },
    LOCAL_RESPONSE: function (state, response) {
      if (response.ok === true) {
        console.log('Guardado correctamente')
      }
    },
    LOCAL_IMAGE_RESPONSE: function (state, response) {
      state.data_locales[state.current_local_edit.id1].foto = response.body.foto
    },
    // ----------Fail-----------
    ASISTENTE_API_FAIL: function (error) {
      console.log(error)
    }

  }
})

export default store
