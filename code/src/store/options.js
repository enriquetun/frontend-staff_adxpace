window.moment = require('moment')
const moment = require('moment')

const options = {
  table_options: {
    headings: {
      // 'username': 'Username',
      'fecha_de_registro': 'Fecha de Registro',
      'propietario': 'Pk de propietario asignado'
    },
    texts: {
      filter: '',
      filterBy: '{column}'
    },
    headingsTooltips: {
      'nombre': 'Filtrar por nombre'
    },
    dateFormat: 'DD-MM-YYYY',
    dateColumns: ['fecha_de_registro'],
    toMomentFormat: true,
    datepickerOptions: {
      autoUpdateInput: true,
      autoApply: true,
      expanded: true,
      showDropdowns: true,
      opens: 'center',
      drops: 'down',
      locale: {
        cancelLabel: 'Cancelar',
        applyLabel: 'Seleccionar'
      },
      timeZone: null,
      firstDayOfWeek: 1,
      startDate: moment().startOf('year'),
      endDate: moment(),
      ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Ultimos 7 dias': [moment().subtract(6, 'days'), moment()],
        'Ultimos 30 dias': [moment().subtract(29, 'days'), moment()],
        'Este mes': [moment().startOf('month'), moment().endOf('month')],
        'Mes anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'Este año': [moment().startOf('year'), moment()],
        'Año anterior': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
      }
    },
    showModal: false,
    footerHeadings: false,
    columnsDropdown: false,
    filterByColumn: true,
    filterable: [
      'pk',
      // 'username',
      // 'propietario',
      'nombre',
      'giro',
      'fecha_de_registro'
    ],
    sortable: [
      'pk',
      // 'username',
      // 'propietario',
      'nombre',
      'giro',
      'fecha_de_registro'
    ],
    childRowTogglerFirst: false,
    pagination: {
      chunk: 25,
      dropdown: true,
      esdge: true
    },
    sortIcon: {
      is: 'fas fa-arrows-alt-v',
      up: 'fas fa-sort-amount-up',
      down: 'fas fa-sort-amount-down'
    },
    preserveState: false,
    saveState: false,
    highlightMatches: true,
    perPage: 15,
    perPageValues: [10, 25, 50, 100, 200, 1000]
  }
}

export default options
