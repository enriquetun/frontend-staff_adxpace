import Vue from 'vue'
import VueResource from 'vue-resource'
import store from './store.js'
import jQuery from 'jquery'

Vue.use(VueResource)

export default {
  get (url) {
    return Vue.http.get(url)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  },
  post (url, request) {
    request.csrfmiddlewaretoken = getCookie('csrftoken')
    var config = {emulateJSON: true}
    return Vue.http.post(url, request, config)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  },
  post_file (url, request) {
    request.append('csrfmiddlewaretoken', getCookie('csrftoken'))
    request.append('username', store.state.username)
    var config =
    {headers: {'Content-Type': 'multipart/form-data'}
    }

    return Vue.http.post(url, request, config)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  },
  post_json (url, request) {
    request.csrfmiddlewaretoken = getCookie('csrftoken')
    var config =
    {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    }

    return Vue.http.post(url, request, config)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  },
  patch (url, request) {
    request.csrfmiddlewaretoken = getCookie('csrftoken')
    var config = {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'X-CSRFToken': request.csrfmiddlewaretoken
      }
    }
    return Vue.http.patch(url, request, config)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  },
  delete (url, request) {
    return Vue.http.delete(url, request)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  }
}

function getCookie (name) {
  var cookieValue = null
  if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';')
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i])
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
        break
      }
    }
  }
  return cookieValue
}
